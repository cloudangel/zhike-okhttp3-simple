/**
 * huize Service Inc
 * All Rights Reserved @2018
 */
package com.hzins.zhike.okhttp3.simple.test;

import com.hzins.zhike.okhttp3.simple.InitOkHttpParam;
import com.hzins.zhike.okhttp3.simple.SimpleOkHttpUtils;
import com.hzins.zhike.okhttp3.simple.interval.NamedThreadFactory;
import lombok.extern.slf4j.Slf4j;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.concurrent.*;

/**
 * 描述:
 *
 * @author tianyuliang
 * @version $Id: SimpleHttpTest.java, v0.1 2022/4/14
 */
@Slf4j
public class SimpleHttpTest {

    private InitOkHttpParam okHttpParam = new InitOkHttpParam();
    private ThreadPoolExecutor executor = null;

    @Before
    public void setUp() {
        okHttpParam.setReadTimeOut(1000);
        okHttpParam.setConnectTimeOut(1000);
        okHttpParam.setWriteTimeOut(800);
        okHttpParam.setRetryCount(3);
        okHttpParam.setRetryInterval(1000);
        okHttpParam.setMaxRequest(32);
        okHttpParam.setMaxRequestPreHost(128);

        BlockingQueue<Runnable> workQueue = new ArrayBlockingQueue<>(6);
        NamedThreadFactory threadFactory = new NamedThreadFactory();
        RejectedExecutionHandler defaultHandler = new ThreadPoolExecutor.CallerRunsPolicy();
        executor = new ThreadPoolExecutor(3, 5, 7, TimeUnit.SECONDS, workQueue, threadFactory, defaultHandler);
        executor.allowCoreThreadTimeOut(false);
        executor.prestartCoreThread();
        log.info("线程池-初始化完成!");

        okHttpParam.setExecutor(executor);
    }

    @After
    public void setDown() {
        SimpleOkHttpUtils.shutdown(executor);
        SimpleOkHttpUtils.shutdownSelf();
    }


    @Test
    public void test_case1() {
        String url = "http://www.baidu.com";
        String respData = SimpleOkHttpUtils.get(url);
        System.out.println(respData);
    }

}