/*
 * Copyright (c) 2022 Huize (Chengdu) Network Technology Co., LTD All rights reserved.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * huize Service Inc
 * All Rights Reserved @2018
 */
package com.hzins.zhike.okhttp3.simple.interval;

/**
 * 描述:
 *
 * @author tianyuliang
 * @version $Id: ThreadUtil.java, v0.1 2021/2/3
 */
public class ThreadUtil {

    /**
     * 获取当前线程的线程组
     *
     * @return 线程组
     * @since 3.1.2
     */
    public static ThreadGroup currentThreadGroup() {
        final SecurityManager s = System.getSecurityManager();
        return (null != s) ? s.getThreadGroup() : Thread.currentThread().getThreadGroup();
    }


}