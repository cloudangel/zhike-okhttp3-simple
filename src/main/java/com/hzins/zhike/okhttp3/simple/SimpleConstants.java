/**
 * huize Service Inc
 * All Rights Reserved @2018
 */
package com.hzins.zhike.okhttp3.simple;

import okhttp3.MediaType;

/**
 * 描述:
 *
 * @author tianyuliang
 * @version $Id: SimpleConstants.java, v0.1 2022/4/14
 */
public class SimpleConstants {

    public static final MediaType MEDIA_TYPE = MediaType.get("application/json; charset=utf-8");

    public static final int SC_OK = 200;

}