/**
 * huize Service Inc
 * All Rights Reserved @2018
 */
package com.hzins.zhike.okhttp3.simple;

import lombok.Builder;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.concurrent.ThreadPoolExecutor;

/**
 * 描述: OkHttp3初始化参数
 *
 * @author tianyuliang
 * @version $Id: OkHttpInitParam.java, v0.1 2022/4/14
 */
@ToString
@NoArgsConstructor
@Builder
public class InitOkHttpParam {

    /**
     * OkHttp3内置线程池（自定义设置比较友好，不建议使用官方默认值）
     */
    public static ThreadPoolExecutor EXECUTOR;

    /**
     * 最大并发请求数
     */
    public static Integer MAX_REQUEST;

    /**
     * 每个主机最大请求数
     */
    public static Integer MAX_REQUEST_PRE_HOST;

    /**
     * 读超时时间 毫秒
     */
    public static Integer READ_TIME_OUT;

    /**
     * 创建链接超时时间 毫秒
     */
    public static Integer CONNECT_TIME_OUT;

    /**
     * 写超时时间 毫秒
     */
    public static Integer WRITE_TIME_OUT;

    /**
     * 最大重试次数
     */
    public static Integer RETRY_COUNT;

    /**
     * 重试间隔 毫秒
     */
    public static Integer RETRY_INTERVAL;

    public static void setExecutor(ThreadPoolExecutor executor) {
        EXECUTOR = executor;
    }

    public static void setMaxRequest(Integer maxRequest) {
        MAX_REQUEST = maxRequest;
    }

    public static void setMaxRequestPreHost(Integer maxRequestPreHost) {
        MAX_REQUEST_PRE_HOST = maxRequestPreHost;
    }

    public static void setReadTimeOut(Integer readTimeOut) {
        READ_TIME_OUT = readTimeOut;
    }

    public static void setConnectTimeOut(Integer connectTimeOut) {
        CONNECT_TIME_OUT = connectTimeOut;
    }

    public static void setWriteTimeOut(Integer writeTimeOut) {
        WRITE_TIME_OUT = writeTimeOut;
    }

    public static void setRetryCount(Integer retryCount) {
        RETRY_COUNT = retryCount;
    }

    public static void setRetryInterval(Integer retryInterval) {
        RETRY_INTERVAL = retryInterval;
    }


}