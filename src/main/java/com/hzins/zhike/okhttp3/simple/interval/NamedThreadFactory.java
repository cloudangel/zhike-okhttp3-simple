/*
 * Copyright (c) 2022 Huize (Chengdu) Network Technology Co., LTD All rights reserved.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * huize Service Inc
 * All Rights Reserved @2018
 */
package com.hzins.zhike.okhttp3.simple.interval;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * 描述: 线程创建工厂类，此工厂可选配置：
 *
 * <pre>
 * 1. 自定义线程命名前缀
 * 2. 自定义是否守护线程
 * </pre>
 *
 * @author tianyuliang
 * @version $Id: NamedThreadFactory.java, v0.1 2021/2/3
 */
public class NamedThreadFactory implements ThreadFactory {

    /**
     * 线程名称 默认前缀
     */
    private static final String DEFAULT_PREFIX = "biz";

    /**
     * 命名前缀
     */
    private final String prefix;
    /**
     * 线程组
     */
    private final ThreadGroup group;
    /**
     * 线程组
     */
    private final AtomicInteger threadNumber = new AtomicInteger(1);
    /**
     * 是否守护线程
     */
    private final boolean isDaemon;
    /**
     * 无法捕获的异常统一处理
     */
    private final Thread.UncaughtExceptionHandler handler;

    /**
     * 构造
     */
    public NamedThreadFactory() {
        this(null, false);
    }

    /**
     * 构造
     *
     * @param isDaemon 是否守护线程
     */
    public NamedThreadFactory(boolean isDaemon) {
        this(null, isDaemon);
    }


    /**
     * 构造
     *
     * @param prefix   线程名前缀
     * @param isDaemon 是否守护线程
     */
    public NamedThreadFactory(String prefix, boolean isDaemon) {
        this(prefix, null, isDaemon);
    }

    /**
     * 构造
     *
     * @param prefix      线程名前缀
     * @param threadGroup 线程组，可以为null
     * @param isDaemon    是否守护线程
     */
    public NamedThreadFactory(String prefix, ThreadGroup threadGroup, boolean isDaemon) {
        this(prefix, threadGroup, isDaemon, null);
    }

    /**
     * 构造
     *
     * @param prefix      线程名前缀
     * @param threadGroup 线程组，可以为null
     * @param isDaemon    是否守护线程
     * @param handler     未捕获异常处理
     */
    public NamedThreadFactory(String prefix, ThreadGroup threadGroup, boolean isDaemon, Thread.UncaughtExceptionHandler handler) {
        this.prefix = (prefix == null || prefix.trim().length() == 0) ? DEFAULT_PREFIX : prefix;
        if (null == threadGroup) {
            threadGroup = ThreadUtil.currentThreadGroup();
        }
        this.group = threadGroup;
        this.isDaemon = isDaemon;
        this.handler = handler;
    }

    @Override
    public Thread newThread(Runnable runnable) {
        Integer number = threadNumber.getAndIncrement();
        String threadNumber = number <= 9 ? ("0" + number.toString()) : number.toString();
        String threadName = String.format("%s%s", prefix, threadNumber);
        final Thread thread = new Thread(this.group, runnable, threadName);

        //守护线程
        if (false == thread.isDaemon()) {
            if (isDaemon) {
                // 原线程为非守护则设置为守护
                thread.setDaemon(true);
            }
        } else if (false == isDaemon) {
            // 原线程为守护则还原为非守护
            thread.setDaemon(false);
        }
        //异常处理
        if (null != this.handler) {
            thread.setUncaughtExceptionHandler(handler);
        }
        //优先级
        if (Thread.NORM_PRIORITY != thread.getPriority()) {
            // 标准优先级
            thread.setPriority(Thread.NORM_PRIORITY);
        }
        return thread;
    }

}